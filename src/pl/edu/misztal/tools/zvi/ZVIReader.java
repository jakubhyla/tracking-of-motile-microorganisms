package pl.edu.misztal.tools.zvi;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
//import utils.system.OsUtils;

/**
 *
 * @author Krzysztof
 */
public class ZVIReader {

    private static boolean littleEndian=true;
    private static int numC;
    private static Set C_Set=new HashSet(); // to hold C channel index collection
    static String fileName;
    // -- Constants --

    /**
     * First few bytes of every ZVI file.
     */
    private static final byte[] ZVI_SIG={
        -48,-49,17,-32,-95,-79,26,-31
    };

    /**
     * Block identifying start of useful header information.
     */
    private static final byte[] ZVI_MAGIC_BLOCK_1={ // 41 00 10
        65,0,16
    };

    /**
     * Block identifying second part of useful header information.
     */
    private static final byte[] ZVI_MAGIC_BLOCK_2={ // 41 00 80
        65,0,-128
    };

    /**
     * Block identifying third part of useful header information.
     */
    private static final byte[] ZVI_MAGIC_BLOCK_3={ // 20 00 10
        32,0,16
    };

    /**
     * Memory buffer size in bytes, for reading from disk.
     */
    private static final int BUFFER_SIZE=8192;

    /**
     * Debugging flag.
     */
    private static final boolean DEBUG=false;

    public static ImageStack load(String filename) {
        FileInfo[] fi=null;
        try {
            fi=getHeaderInfo(filename);
        }
        catch (IOException e) {
            System.err.println("");
            System.err.println("ZVI Reader"+" "+e);
            return null;
        }
        if (fi==null)
            return null;
        
        return openTiffStack(fi);
    }

    public static ArrayList<BufferedImage> loadToBufferedImages(String filemane) {
        System.out.println(filemane);
        return toImages(load(filemane));
    }

    /**
     * Are all the images in this file the same size and type?
     */
    private static boolean allSameSizeAndType(FileInfo[] info) {
        boolean sameSizeAndType=true;
        boolean contiguous=true;
        long startingOffset=info[0].getOffset();
        int size=info[0].width*info[0].height*info[0].getBytesPerPixel();
        for (int i=1; i<info.length; i++) {
            sameSizeAndType&=info[i].fileType==info[0].fileType
                    &&info[i].width==info[0].width
                    &&info[i].height==info[0].height;
            contiguous&=info[i].getOffset()==startingOffset+i*size;
        }
        if (contiguous&&info[0].fileType!=FileInfo.RGB48)
            info[0].nImages=info.length;
        return sameSizeAndType;
    }

    /**
     * Returns an InputStream for the image described by this FileInfo.
     */
    private static InputStream createInputStream(FileInfo fi) throws IOException,MalformedURLException {
        if (fi.inputStream!=null)
            return fi.inputStream;
        else if (fi.url!=null&&!fi.url.equals(""))
            return new URL(fi.url+fi.fileName).openStream();
        else {
            File f=new File(fi.directory+fi.fileName);
            if (f==null||f.isDirectory())
                return null;
            else {
                InputStream is=new FileInputStream(f);
                if (fi.compression>=FileInfo.LZW)
                    is=new RandomAccessStream(is);
                return is;
            }
        }
    }

    /**
     * Returns an IndexColorModel for the image specified by this FileInfo.
     */
    private static ColorModel createColorModel(FileInfo fi) {
        if (fi.lutSize>0)
            return new IndexColorModel(8,fi.lutSize,fi.reds,fi.greens,fi.blues);
        else
            return LookUpTable.createGrayscaleColorModel(fi.whiteIsZero);
    }

    /**
     * Attemps to open a tiff file as a stack. Returns an ImagePlus object if
     * successful.
     *
     * @param info
     * @return
     */
    private static ImageStack openTiffStack(FileInfo[] info) {
        if (info.length>1&&!allSameSizeAndType(info))
            return null;
        FileInfo fi=info[0];
        if (fi.nImages>1)// open contiguous images as stack
        
            throw new RuntimeException("Not Implemented Yet");
        else {
            ColorModel cm=createColorModel(fi);
            ImageStack stack=new ImageStack(fi.width,fi.height,cm);
            Object pixels=null;
            long skip=fi.getOffset();
            int imageSize=fi.width*fi.height*fi.getBytesPerPixel();
//            if (info[0].fileType == FileInfo.GRAY12_UNSIGNED) {
//                imageSize = (int) (fi.width * fi.height * 1.5);
//                if ((imageSize & 1) == 1) {
//                    imageSize++; // add 1 if odd
//                }
//            }
//            if (info[0].fileType == FileInfo.BITMAP) {
//                int scan = (int) Math.ceil(fi.width / 8.0);
//                imageSize = scan * fi.height;
//            }
            long loc=0L;
            int nChannels=1;
            try {
                try (InputStream is=createInputStream(fi)) {
                    ImageReader reader=new ImageReader(fi);
                    for (int i=0; i<info.length; i++) {
                        nChannels=1;
                        Object[] channels=null;
                        if (info[i].compression>=FileInfo.LZW) {
                            fi.stripOffsets=info[i].stripOffsets;
                            fi.stripLengths=info[i].stripLengths;
                        }
                        int bpp=info[i].getBytesPerPixel();
                        if (info[i].samplesPerPixel>1&&!(bpp==3||bpp==4||bpp==6)) {
                            nChannels=fi.samplesPerPixel;
                            channels=new Object[nChannels];
                            for (int c=0; c<nChannels; c++) {
                                pixels=reader.readPixels(is,c==0?skip:0L);
                                channels[c]=pixels;
                            }
                        }
                        else
                            pixels=reader.readPixels(is,skip);
                        if (pixels==null&&channels==null)
                            break;
                        loc+=imageSize*nChannels+skip;
                        if (i<(info.length-1)) {
                            skip=info[i+1].getOffset()-loc;
                            if (info[i+1].compression>=FileInfo.LZW)
                                skip=0;
                            if (skip<0L)
                                break;
                        }
                        if (fi.fileType==FileInfo.RGB48) {
                            Object[] pixels2=(Object[])pixels;
                            stack.addSlice("image-"+i,pixels2[0]);
                            stack.addSlice("image-"+i,pixels2[1]);
                            stack.addSlice("image-"+i,pixels2[2]);
                        }
                        else if (nChannels>1) {
                            for (int c=0; c<nChannels; c++)
                                if (channels[c]!=null)
                                    stack.addSlice("image-"+i,channels[c]);
                        }
                        else
                            stack.addSlice("image-"+i,pixels);
                    }
                }
            }
            catch (Exception|OutOfMemoryError e) {
                System.err.println("ZVI Reader: "+e);
            }
            if (stack.getSize()==0)
                return null;
            return stack;
        }
    }

    // -- Helper methods --
    /**
     * Reads header information from the given file.
     */
    private static FileInfo[] getHeaderInfo(String fileName)
            throws IOException {
        // Highly questionable decoding strategy:
        //
        // Note that all byte ordering is little endian, including 4-byte header
        // fields. Other examples: 16-bit data is LSB MSB, and 3-channel data is
        // BGR instead of RGB.
        //
        // 1) Find image header byte sequence:
        //    A) Find 41 00 10. (ZVI_MAGIC_BLOCK_1)
        //    B) Skip 19 bytes of stuff.
        //    C) Read 41 00 80. (ZVI_MAGIC_BLOCK_2)
        //    D) Read 11 bytes of 00.
        //    E) Read potential header information:
        //       - Z-slice (4 bytes)
        //       - channel (4 bytes)
        //       - timestep (4 bytes)
        //    F) Read 108 bytes of 00.
        //
        // 2) If byte sequence is not as expected at any point (e.g.,
        //    stuff that is supposed to be 00 isn't), start over at 1A.
        //
        // 3) Find 20 00 10. (ZVI_MAGIC_BLOCK_3)
        //
        // 4) Read more header information:
        //    - width (4 bytes)
        //    - height (4 bytes)
        //    - ? (4 bytes; always 1)
        //    - bytesPerPixel (4 bytes)
        //    - pixelType (this is what the AxioVision software calls it)
        //       - 1=24-bit (3 color components, 8-bit each)
        //       - 3=8-bit (1 color component, 8-bit)
        //       - 4=16-bit (1 color component, 16-bit)
        //       - 8=48-bit (3 color components, 16-bit each)
        //    - bitDepth (4 bytes--usually, but not always, bytesPerPixel * 8)
        //       - AK: seems that it is always 16
        //
        // 5) Read image data (width * height * bytesPerPixel)
        //
        // 6) Repeat the entire process until no more headers are identified.

        RandomAccessFile in=new RandomAccessFile(fileName,"r");
        byte[] sig=new byte[ZVI_SIG.length];
        in.readFully(sig);
        for (int i=0; i<sig.length; i++)
            if (sig[i]!=ZVI_SIG[i])
                return null;
        Set Z_Set=new HashSet(); // to hold Z plan index collection.
        Set T_Set=new HashSet(); // to hold T time index collection.
        long pos=0;
        ArrayList<ZVIBlock> blockList=new ArrayList();
        int numZ=0, numT=0;
        numC=0;
        while (true) {
            // search for start of next image header
            long header=findBlock(in,ZVI_MAGIC_BLOCK_1,pos);

            if (header<0)
                // no more potential headers found; we're done
                break;
            pos=header+ZVI_MAGIC_BLOCK_1.length;

            if (DEBUG)
                System.err.println("Found potential image block: "+header);

            // these bytes don't matter
            in.skipBytes(19);
            pos+=19;

            // these bytes should match ZVI_MAGIC_BLOCK_2
            byte[] b=new byte[ZVI_MAGIC_BLOCK_2.length];
            in.readFully(b);
            boolean ok=true;
            for (int i=0; i<b.length; i++) {
                if (b[i]!=ZVI_MAGIC_BLOCK_2[i]) {
                    ok=false;
                    break;
                }
                pos++;
            }
            if (!ok)
                continue;

            // these bytes should be 00
            b=new byte[11];
            in.readFully(b);
            for (int i=0; i<b.length; i++) {
                if (b[i]!=0) {
                    ok=false;
                    break;
                }
                pos++;
            }
            if (!ok)
                continue;

            // read potential header information
            int theZ=readInt(in);
            int theC=readInt(in);
            int theT=readInt(in);
            pos+=12;

            // these bytes should be 00
            b=new byte[108];
            in.readFully(b);
            for (int i=0; i<b.length; i++) {
                if (b[i]!=0) {
                    ok=false;
                    break;
                }
                pos++;
            }
            if (!ok)
                continue;
            // everything checks out; looks like an image header to me

            //      Some zvi images have the following structure:
            //        ZVI_SIG                    Decoding:
            //        ZVI_MAGIC_BLOCK_1 
            //        ZVI_MAGIC_BLOCK_2      <== Start of header information
            //        - Z-slice (4 bytes)     -> theZ = 0
            //        - channel (4 bytes)     -> theC = 0
            //        - timestep (4 bytes)    -> theT = 0
            //        ZVI_MAGIC_BLOCK_2      <==  Start of header information
            //        - Z-slice (4 bytes)     -> theZ actual value
            //        - channel (4 bytes)     -> theC actual value
            //        - timestep (4 bytes)    -> theT actual value
            //        ZVI_MAGIC_BLOCK_3      <== End of header information
            //        ... 
            //        
            //        Two consecutive Start of header information ZVI_MAGIC_BLOCK_2
            //        make test 3) of original decoding strategy fail. The first
            //        null values are taken as theZ, theC and theT values, the
            //        following actual values are ignored. 
            //        Parsing the rest of the file appears to be ok.
            //
            //        New decoding strategy looks for the last header information
            //        ZVI_MAGIC_BLOCK_2 / ZVI_MAGIC_BLOCK_3 to get proper image
            //        slice theZ, theC and theT values.
            //- original code removed
            //- long magic3 = findBlock(in, ZVI_MAGIC_BLOCK_3, pos);
            //- if (magic3 < 0) return null;
            //- pos = magic3 + ZVI_MAGIC_BLOCK_3.length;
            //- 
            //-
            //+ new code
            // these bytes don't matter
            in.skipBytes(89);
            pos+=89;

            byte[] magic3=new byte[ZVI_MAGIC_BLOCK_3.length];
            in.readFully(magic3);
            for (int i=0; i<magic3.length; i++)
                if (magic3[i]!=ZVI_MAGIC_BLOCK_3[i]) {
                    ok=false;
                    break;
                }
            if (!ok)
                continue;
            pos+=ZVI_MAGIC_BLOCK_3.length;

            // read more header information
            int w=readInt(in);
            int h=readInt(in);
            int alwaysOne=readInt(in); // don't know what this is for
            int bytesPerPixel=readInt(in);
            int pixelType=readInt(in); // not clear what this value signifies
            int bitDepth=readInt(in); // doesn't always equal bytesPerPixel * 8
            pos+=24;

            ZVIBlock zviBlock=new ZVIBlock(theZ,theC,theT,
                                           w,h,alwaysOne,bytesPerPixel,pixelType,bitDepth,pos);
            if (DEBUG)
                System.out.println(zviBlock);
            Z_Set.add(new Integer(theZ));
            C_Set.add(new Integer(theC));
            T_Set.add(new Integer(theT));
            // save this image block's position
            blockList.add(zviBlock);
            pos+=w*h*bytesPerPixel;
        }

        if (blockList.isEmpty())
            return null;

        numZ=Z_Set.size();
        numC=C_Set.size();
        numT=T_Set.size();

        if (numZ*numC*numT!=blockList.size())
            System.err.println("ZVI Reader "+"Warning: image counts do not match.");

        // convert ZVI blocks into single FileInfo object
        FileInfo[] fi=new FileInfo[blockList.size()];
        for (int i=0; i<fi.length; i++) {
            ZVIBlock zviBlock=(ZVIBlock)blockList.get(i);
            int ft=-1;
            if (zviBlock.numChannels==1) {
                if (zviBlock.bytesPerChannel==1)
                    ft=FileInfo.GRAY8;
                else if (zviBlock.bytesPerChannel==2)
                    ft=FileInfo.GRAY16_UNSIGNED;
            }
            else if (zviBlock.numChannels==3)
                if (zviBlock.bytesPerChannel==1)
                    ft=FileInfo.BGR;
                else if (zviBlock.bytesPerChannel==2)
                    ft=FileInfo.RGB48;
            if (ft<0) {
                System.err.println("ZVIReader "
                        +"Warning: unknown file type for image plane #"+(i+1));
                ft=FileInfo.GRAY8; // better than nothing...
            }
            fi[i]=new FileInfo();
            fi[i].fileFormat=FileInfo.RAW;
            fi[i].fileName=new File(fileName).getName();

            fi[i].directory=new File(fileName).getParent()+File.separator;
//            if(OsUtils.isLinux())
//                fi[i].directory = new File(fileName).getParent() + "/";
//            else if(OsUtils.isWindows())
//                fi[i].directory = new File(fileName).getParent() + "\\";

            fi[i].nImages=1;
            fi[i].intelByteOrder=true;
            fi[i].width=zviBlock.width;
            fi[i].height=zviBlock.height;
            fi[i].offset=(int)zviBlock.imagePos;
            fi[i].fileType=ft;

            // add Z, C and T index to file info.
            fi[i].info=zviBlock.theZ+","+zviBlock.theC+","+zviBlock.theT;
        }
        return fi;
    }

    /**
     * Reads a little-endian integer from the given file.
     */
    private static int readInt(RandomAccessFile fin) throws IOException {
        byte[] b=new byte[4];
        fin.readFully(b);
        return batoi(b);
    }

    /**
     * Translates up to the first 4 bytes of a byte array to an integer.
     */
    private static int batoi(byte[] b) {
        int len=b.length>4?4:b.length;
        int total=0;
        for (int i=0; i<len; i++) {
            int q=b[i]<0?b[i]+256:b[i]; // convert to unsigned
            int shift=8*i; // little endian
            total+=q<<shift;
        }
        return total;
    }

    /**
     * Finds the first occurrence of the given byte block within the file,
     * starting from the given file position.
     */
    private static long findBlock(RandomAccessFile in,byte[] block,long start)
            throws IOException {
        long filePos=start;
        long fileSize=in.length();
        byte[] buf=new byte[BUFFER_SIZE];
        long spot=-1;
        int step=0;
        boolean found=false;
        in.seek(start);

        while (true) {
            int len=(int)(fileSize-filePos);
            if (len<0)
                break;
            if (len>buf.length)
                len=buf.length;
            in.readFully(buf,0,len);

            for (int i=0; i<len; i++)
                if (buf[i]==block[step]) {
                    if (step==0)
                        // could be a match; flag this spot
                        spot=filePos+i;
                    step++;
                    if (step==block.length) {
                        // found complete match; done searching
                        found=true;
                        break;
                    }
                }
                else {
                    // no match; reset step indicator
                    spot=-1;
                    step=0;
                }
            if (found)
                break; // found a match; we're done
            if (len<buf.length)
                break; // EOF reached; we're done
            filePos+=len;
        }

        // set file pointer to byte immediately following pattern
        if (spot>=0)
            in.seek(spot+block.length);

        return spot;
    }

    private static ArrayList<BufferedImage> toImages(ImageStack stack) {
        ArrayList<BufferedImage> ret=new ArrayList();
        for (int i=0; i<stack.getSize(); ++i) {
            final String label=stack.getSliceLabel(i+1);
            int k=1;
            while ((i+1+k<stack.getSize())&&stack.getSliceLabel(i+1+k++).equals(label));
            --k;
            if (k>1)
                throw new RuntimeException("Not Implemented Yet");
            else {
                BufferedImage m=new BufferedImage(stack.getWidth(),stack.getHeight(),BufferedImage.TYPE_BYTE_GRAY);
                byte[] pix=(byte[])stack.getPixels(i+1);

                WritableRaster raster=m.getRaster();
                for (int j=0; j<pix.length; j++)
                    raster.setSample(j%stack.getWidth(),j/stack.getWidth(),0,pix[j]&0xff);

                m.setRGB(i,i,i);
                ret.add(m);
            }
        }
        return ret;
    }

    /**
     * Contains information collected from a ZVI image header.
     */
    private static class ZVIBlock {

        private int theZ, theC, theT;
        private int width, height;
        private final int alwaysOne;
        private final int bytesPerPixel;
        private final int pixelType;
        private final int bitDepth;
        private long imagePos;

        private final int numPixels;
        private final int imageSize;
        private int numChannels;
        private int bytesPerChannel;

        public ZVIBlock(int theZ,int theC,int theT,int width,int height,
                        int alwaysOne,int bytesPerPixel,int pixelType,int bitDepth,
                        long imagePos) {
            this.theZ=theZ;
            this.theC=theC;
            this.theT=theT;
            this.width=width;
            this.height=height;
            this.alwaysOne=alwaysOne;
            this.bytesPerPixel=bytesPerPixel;
            this.pixelType=pixelType;
            this.bitDepth=bitDepth;
            this.imagePos=imagePos;

            numPixels=width*height;
            imageSize=numPixels*bytesPerPixel;

            numChannels=1;
            // the second decision is redundant, but left there for further(?) pixel types 
            if ((pixelType==1)|(pixelType==8))
                numChannels=3; // 1 and 8 are RGB 8-bit and 16-bit
            else if ((pixelType==3)|(pixelType==4))
                numChannels=1; // 3 and 4 are GRAY 8-bit and 16-bit

            if (bytesPerPixel%numChannels!=0) {
                System.err.println("ZVI Reader"+"Warning: incompatible bytesPerPixel ("
                        +bytesPerPixel+") and numChannels ("+numChannels
                        +"). Assuming grayscale data.");
                numChannels=1;
            }
            bytesPerChannel=bytesPerPixel/numChannels;
        }

        @Override
        public String toString() {
            return "Image header block:\n"
                    +"  theZ = "+theZ+"\n"
                    +"  theC = "+theC+"\n"
                    +"  theT = "+theT+"\n"
                    +"  width = "+width+"\n"
                    +"  height = "+height+"\n"
                    +"  alwaysOne = "+alwaysOne+"\n"
                    +"  bytesPerPixel = "+bytesPerPixel+"\n"
                    +"  pixelType = "+pixelType+"\n"
                    +"  bitDepth = "+bitDepth;
        }
    }

    /*public static void main(String[] args) {
     try {
     // Set System L&F
     UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
     } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
     // handle exception
     }

     String filename = "d:\\Dropbox\\research\\cells\\seria09_48h_orange_Glass_pbs0min.zvi";
     //        String filename = "d:\\Dropbox\\research\\cells\\seria10_48h_orange_Glass_pbs25min.zvi";
     ArrayList<BufferedImage> m = ZVIReader.loadToBufferedImages(filename);
     for (BufferedImage v : m) {
     Show.show(v);
     }
     }*/
}
