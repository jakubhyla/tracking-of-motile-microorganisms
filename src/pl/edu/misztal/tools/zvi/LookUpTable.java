package pl.edu.misztal.tools.zvi;

import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;

/**
 *
 * @author Krzysztof
 */
/**
 * This class represents a color look-up table.
 */
public class LookUpTable {

    public static ColorModel createGrayscaleColorModel(boolean invert) {
        byte[] rLUT = new byte[256];
        byte[] gLUT = new byte[256];
        byte[] bLUT = new byte[256];
        if (invert) {
            for (int i = 0; i < 256; i++) {
                rLUT[255 - i] = (byte) i;
                gLUT[255 - i] = (byte) i;
                bLUT[255 - i] = (byte) i;
            }
        } else {
            for (int i = 0; i < 256; i++) {
                rLUT[i] = (byte) i;
                gLUT[i] = (byte) i;
                bLUT[i] = (byte) i;
            }
        }
        return (new IndexColorModel(8, 256, rLUT, gLUT, bLUT));
    }

}
