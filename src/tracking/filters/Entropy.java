package tracking.filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

public final class Entropy {
    public static Mat process(Mat img, int x, int y, int width, int height, int r) {
        //img = ImageUtils.getGrayScale(img);
        
        Mat tmp = new Mat(img.width(), img.height(), CvType.CV_8UC1);
                        
        int arrayOfImage[][] = new int[img.width()][img.height()];
        double histogram[] = new double [255];
        
        for(int i=x; i<x+width; ++i) {
            for(int j=y; j<y+height; ++j) {
                arrayOfImage[i][j] = (int) tmp.get(i, j)[0];
            }
        }
                
        for(int i=x+r; i<x+width-r; ++i) {
            for(int j=y+r; j<y+height-r; ++j) {
                double entropy = 0.0;
                
                for(int k=0; k<255; ++k)
                    histogram[k] = 0;
                
                for(int k=0; k<2*r+1; ++k) {
                    for(int h=0; h<2*r+1; ++h) {
                        histogram[arrayOfImage[i+k-r][j+h-r]] ++;
                    }
                }
                
               // System.out.println(histogram[10]);
                
                for(int k=0; k<255; ++k) {
                    if(histogram[k] != 0) {
                        histogram[k] /= (r==1 ? 2 : r*r);
                        entropy += histogram[k]*Math.log((double)histogram[k]);
                    }
                }
                
                if(entropy != 0)
                    entropy = -entropy;
                
                //.out.println(entropy);
                int newColor = (int)(entropy*255.0/2.0);
                if(newColor > 255)
                    newColor = 255;
                else if(newColor < 0)
                    newColor = 0;
                
                
                
                Core.line(tmp,new Point(x,y),new Point(x,y),new Scalar(newColor,newColor,newColor));
                
            }
        }
        
        Mat newImage = new Mat(tmp.width(), tmp.height(), CvType.CV_8UC3);

        return newImage;
    }
}
