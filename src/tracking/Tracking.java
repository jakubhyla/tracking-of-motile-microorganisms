package tracking;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import tracking.view.MainViewController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.opencv.core.Core;
import tracking.utils.StageConfiguration;
import tracking.video.Video;

public class Tracking extends Application {

    public static Stage mainStage;
    public static Stage videoStage;
    public static Video video;

    @Override
    public void start(Stage mainStage) throws Exception {
        Parent mainView=(Parent)FXMLLoader.load(getClass().getResource("view/MainView.fxml"));

        MainViewController.setStage(mainStage);

        Scene scene=new Scene(mainView);

        mainStage=StageConfiguration.initializeStage(mainStage,scene,"Tracking of motile microorganisms","/res/bacteria-icon.png",false);
        this.mainStage=mainStage;
        mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (videoStage!=null)
                    videoStage.close();
            }
        });
        this.mainStage.show();

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {
        launch(args);
    }

}
