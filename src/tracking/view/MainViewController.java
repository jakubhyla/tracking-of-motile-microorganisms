package tracking.view;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import tracking.Tracking;
import static tracking.Tracking.videoStage;
import tracking.utils.StageConfiguration;

import tracking.video.Video;

public class MainViewController implements Initializable {

    @FXML
    private TextArea aboutField;
    @FXML
    private TextField fileField, fpsField, widthField, heightField;
    @FXML
    private Parent root;
    @FXML
    private ImageView imageView;

    private static Stage primaryStage;

    @Override
    public void initialize(URL url,ResourceBundle rb) {

    }

    @FXML
    protected void handleLoadBtn(ActionEvent event) {

        FileChooser fileChooser=new FileChooser();
        FileChooser.ExtensionFilter extFilter=new FileChooser.ExtensionFilter("ZVI File (*.zvi)","*.zvi");
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.getExtensionFilters().clear();
        fileChooser.getExtensionFilters().add(extFilter);
        File file=fileChooser.showOpenDialog(primaryStage);

        if (file!=null) {
            fileField.setText(file.getPath());
            Tracking.video=new Video(file.getPath());
            Image image=Tracking.video.getImage(0);
            imageView.setImage(image);

            widthField.setText(Tracking.video.getWidth()/2+"");
            heightField.setText(Tracking.video.getHeight()/2+"");

            String text="Frames: "+Tracking.video.getNumOfFrames()+"\n"
                    +"Width: "+Tracking.video.getWidth()+"px\n"
                    +"Height: "+Tracking.video.getHeight()+"px\n";
            aboutField.setText(text);
        }
    }

    @FXML
    protected void handleDoneBtn(ActionEvent event) throws Exception {
        FXMLLoader fxmlLoader=new FXMLLoader();
        Parent videoView=(Parent)fxmlLoader.load(getClass().getResource("VideoView.fxml"));

        Scene scene=new Scene(videoView);

        Tracking.video.setFPS(Integer.parseInt(fpsField.getText()));
        Tracking.video.setCustomHeight(Integer.parseInt(heightField.getText()));
        Tracking.video.setCustomWidth(Integer.parseInt(widthField.getText()));
        System.out.println(Tracking.video.getCustomHeight()+" "+Tracking.video.getCustomWidth());

        Tracking.videoStage=new Stage();
        Tracking.videoStage=StageConfiguration.initializeStage(Tracking.videoStage,scene,"Tracking of motile microorganisms | video","/res/bacteria-icon.png",true);
        Tracking.videoStage.setMinHeight(Double.parseDouble(heightField.getText())+130);
        Tracking.videoStage.setMinWidth(Double.parseDouble(widthField.getText())+420);
        Tracking.videoStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                VideoViewController.appIsRunning=false;
            }
        });

        Tracking.videoStage.show();
    }

    public static void setStage(Stage stage) {
        primaryStage=stage;
    }

}
