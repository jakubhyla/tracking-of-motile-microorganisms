/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tracking.view;

import java.net.URL;
import java.time.Duration;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import tracking.Tracking;
import static tracking.Tracking.videoStage;

/**
 *
 * @author Hamish
 */
public class VideoViewController implements Initializable {

    public static boolean appIsRunning=true;
    @FXML
    public Canvas videoPreview;

    @FXML
    public Slider frameSlider;

    private static boolean play=false;
    private static int frame=0;

    @Override
    public void initialize(URL url,ResourceBundle rb) {
        frameSlider.setMin(0);
        frameSlider.setMax(Tracking.video.getNumOfFrames()-1);
        frameSlider.setValueChanging(true);
        frameSlider.setValue(frame);

        GraphicsContext gc=videoPreview.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0,0,videoPreview.getWidth(),videoPreview.getHeight());

        Task task=new Task<Void>() {
            @Override
            public Void call() throws Exception {
                while (appIsRunning) {
                    if ((frameSlider.getValue()+1)%Tracking.video.getNumOfFrames()!=frame) {
                        frame=(int)frameSlider.getValue();

                        if (!play)
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    gc.drawImage(getImage(frame),0,0);
                                    frameSlider.setValueChanging(true);
                                    frameSlider.setValue(frame);
                                }
                            });
                    }

                    if (play) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                gc.drawImage(getImage(frame),0,0);
                                frameSlider.setValueChanging(true);
                                frameSlider.setValue(frame);
                            }
                        });
                        frame++;
                        frame%=Tracking.video.getNumOfFrames();
                    }
                    Thread.sleep((long)(1.0/Tracking.video.getFPS()*1000));
                }
                return null;
            }
        };
        Thread th=new Thread(task);
        th.setDaemon(true);
        th.start();
    }

    public Image getImage(int i) {
        return Tracking.video.getImage(i,Tracking.video.getCustomWidth(),Tracking.video.getCustomHeight());
    }

    @FXML
    protected void handleFirstBtn(ActionEvent event) throws Exception {
        frameChange(0);
    }

    @FXML
    protected void handlePreviousBtn(ActionEvent event) throws Exception {
        frameChange(frame-1);
    }

    @FXML
    protected void handlePlayBtn(ActionEvent event) throws Exception {
        if (((Button)event.getSource()).getId().equals("btn_play")) {
            ((Button)event.getSource()).setId("btn_pause");
            play=true;
        }
        else {
            play=false;
            ((Button)event.getSource()).setId("btn_play");
        }
    }

    @FXML
    protected void handleNextBtn(ActionEvent event) throws Exception {
        frameChange(frame+1);
    }

    @FXML
    protected void handleLastBtn(ActionEvent event) throws Exception {
        frameChange((int)frameSlider.getMax());
    }

    public void frameChange(int v) {
        frame=v;
        frameSlider.setValueChanging(true);
        frameSlider.setValue(frame);
    }
}
