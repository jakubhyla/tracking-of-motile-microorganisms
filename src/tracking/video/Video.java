package tracking.video;

import com.sun.javafx.tk.Toolkit;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import pl.edu.misztal.tools.zvi.ZVIReader;
import tracking.filters.Entropy;

public class Video {

    private ArrayList<Mat> frames;
    private ArrayList<BufferedImage> rawFrames;
    private int customWidth, customHeight, fps;

    public Video(String path) {
        rawFrames=ZVIReader.loadToBufferedImages(path);
        frames=new ArrayList<Mat>();
        for (BufferedImage b:rawFrames) {
            byte[] pixels=((DataBufferByte)b.getRaster().getDataBuffer()).getData();
            Mat image=new Mat(b.getHeight(),b.getWidth(),CvType.CV_8UC1);
            image.put(0,0,pixels);
            frames.add(image);
        }
        
        Mat result = Entropy.process(frames.get(0), 0, 0, frames.get(0).width(), frames.get(0).height(), 2);
        Highgui.imwrite("lol.png",result);
    }

    public Image getImage(int i) {
        return SwingFXUtils.toFXImage(rawFrames.get(i),null);
    }

    public Image getImage(int i,int w,int h) {
        BufferedImage resized=new BufferedImage(w,h,rawFrames.get(i).getType());
        Graphics2D g=resized.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(rawFrames.get(i),0,0,w,h,0,0,rawFrames.get(i).getWidth(),rawFrames.get(i).getHeight(),null);
        g.dispose();

        return SwingFXUtils.toFXImage(resized,null);
    }

    public Image mat2Img(Mat m) {
        int type=BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels()>1)
            type=BufferedImage.TYPE_3BYTE_BGR;
        int bufferSize=m.channels()*m.cols()*m.rows();
        byte[] b=new byte[bufferSize];
        m.get(0,0,b); // get all the pixels
        BufferedImage image=new BufferedImage(m.cols(),m.rows(),type);
        final byte[] targetPixels=((DataBufferByte)image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b,0,targetPixels,0,b.length);

        WritableImage wr=null;
        if (image!=null) {
            wr=new WritableImage(image.getWidth(),image.getHeight());
            PixelWriter pw=wr.getPixelWriter();
            for (int x=0; x<image.getWidth(); x++)
                for (int y=0; y<image.getHeight(); y++)
                    pw.setArgb(x,y,image.getRGB(x,y));
        }
        return wr;

    }

    public void setCustomHeight(int h) {
        customHeight=h;
    }

    public void setCustomWidth(int w) {
        customWidth=w;
    }

    public void setFPS(int fps) {
        this.fps=fps;
    }

    public int getCustomHeight() {
        return customHeight;
    }

    public int getCustomWidth() {
        return customWidth;
    }

    public int getFPS() {
        return fps;
    }

    public Mat getMatImage(int i) {
        return frames.get(i);
    }

    public int getNumOfFrames() {
        return frames.size();
    }

    public int getWidth() {
        return frames.get(0).width();
    }

    public int getHeight() {
        return frames.get(0).height();
    }
}
