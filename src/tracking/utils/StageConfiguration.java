package tracking.utils;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class StageConfiguration {

    public static Stage initializeStage(Stage stage,Scene scene,String title,String icon,boolean resizable) {
        stage.setScene(scene);
        stage.setTitle(title);
        stage.setResizable(resizable);
        stage.getIcons().add(new Image(icon));
        return stage;
    }
}
